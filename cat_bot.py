import random
import requests
import io
import json

from telegram.ext import Updater
from telegram import Update
from telegram.ext import CallbackContext
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters

import local_settings as settings
from yandex_gpt import yandex

# сюда вставьте свой токен из телеграма
bot_token = settings.BOT_TOKEN


updater = Updater(token=bot_token, use_context=True)
dispatcher = updater.dispatcher

MY_STUDENTS_CHAT_ID = '-572550782'


def get_cat_serpapi():
    """
    Функция получения рандомного котика. Для ее работы необходимо завести аккаунт на serpapi.com
    и вставить токен в api_key
    :return:
    """
    api_key = settings.SERP_API_KEY
    query = "кот"
    url = f"https://serpapi.com/search.json?q={query}&tbm=isch&ijn=0&api_key={api_key}"

    response = requests.get(url)
    return response


def get_unsplash_data(query: str):
    api_key = settings.UNSPLASH_ACCESS_KEY
    url = f"https://api.unsplash.com/photos/random"

    response = requests.get(url, params={"query": query, "client_id": api_key})
    return response.json()['urls']['small']

def get_partner(update: Update, context: CallbackContext):
    if str(update.effective_chat.id) == MY_STUDENTS_CHAT_ID: 
        with io.open(f'{settings.DATABSE_FILE_PATH}', encoding='utf-8') as file:
            data = json.loads(file.read())
            partner = random.choice(data)
        context.bot.send_message(chat_id=update.effective_chat.id, text=f"твой напарник: {partner['name']}")


def hello(update: Update, context: CallbackContext):
    """Функция для ответа на команду /start"""
    text = "мяу"
    context.bot.send_message(chat_id=update.effective_chat.id, text=text)


def cat(update: Update, context: CallbackContext):
    """Функция для ответа на сообщение 'какой кот я сегодня?'"""
    response = get_cat_serpapi()
    if response.json().get('images_results'):
        random_cat = random.randint(0, len(response.json()['images_results']))
        image_url = response.json()['images_results'][random_cat]['thumbnail']
        return context.bot.send_photo(chat_id=update.effective_chat.id, photo=image_url)
    else:
        return context.bot.send_photo(chat_id=update.effective_chat.id, photo=get_unsplash_data("кот"))


def dog(update: Update, context: CallbackContext):
    """Функция для ответа на сообщение 'какой пес я сегодня?'"""
    return context.bot.send_photo(chat_id=update.effective_chat.id, photo=get_unsplash_data("собака"))

def emoji(update: Update, context: CallbackContext):
    all_emojies = requests.get(f"https://emoji-api.com/emojis?access_key={settings.EMOJI_API_TOKEN}")
    emoji = random.choice(all_emojies.json())
    context.bot.send_message(chat_id=update.effective_chat.id, text=emoji['character'])


def get_diploma(update: Update, context: CallbackContext):
    red = 'https://www.viacademia.ru/images/2022/diploma-high-dvz-80.jpg'
    blue = 'https://www.viacademia.ru/images/2019/diploma-blue-hight-viacademia.jpg'
    context.bot.send_photo(chat_id=update.effective_chat.id, photo=random.choice([red, blue]))


def get_story(update: Update, context: CallbackContext):
    all_emojies = requests.get(f"https://emoji-api.com/emojis?access_key={settings.EMOJI_API_TOKEN}")
    story = []
    for i in range(random.randint(2, 5)):
        emoji = random.choice(all_emojies.json())
        story += emoji['character']
    context.bot.send_message(chat_id=update.effective_chat.id, text="".join(story))


def cat_gpt(update: Update, context: CallbackContext):
    """Функция для похода в АПИ'"""
    print(update.message.text)
    question = ' '. join(update.message.text.split('нарисуй кота')[1:])
    context.bot.send_message(chat_id=update.effective_chat.id, text=f'рисую, 10 секунд, описание: {question}')
    ans = yandex.generate_image(question)
    if ans:
        print(ans)
        context.bot.send_photo(chat_id=update.effective_chat.id, photo=ans)
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text='не получилось')


# здесь определяем правила для овтета на команды и сообещния
handlers = [
    CommandHandler('hello', hello),
    MessageHandler(Filters.regex(r'нарисуй кота\w*'), cat_gpt),
    MessageHandler(Filters.text('какой кот я сегодня?'), cat),
    MessageHandler(Filters.text('какой пес я сегодня?'), dog),
    MessageHandler(Filters.text('какая емоджи я сегодня?'), emoji),
    MessageHandler(Filters.text('мой напарник сегодня?'), get_partner),
    MessageHandler(Filters.text('диплом'), get_diploma),
    MessageHandler(Filters.text(['история']), get_story),
    CommandHandler('i_am_cat', cat)
]

# подключаем правила ответа на сообщения и команды к нашему боту
for handler in handlers:
    dispatcher.add_handler(handler)

# функция для того, чтобы программа всегда работала и отвечала на сообщения
updater.start_polling()