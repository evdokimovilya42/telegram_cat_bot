# Телеграм бот - "какой ты кот?"

## Инструкция по установке

1. Установить python
2. Склонировать репозиторий командой `git clone git@gitlab.com:evdokimovilya42/telegram_cat_bot.git`
3. Установить библиотеки командой `pip install requirements.txt`
4. В корне проекта создать файлик "local_settings.py" на основе "local_settings_template.py" и вставить в него свои настройки
5. Запустить файлик cat_bot.py
