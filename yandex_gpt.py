import random
import requests
import time
import local_settings as settings
import base64
import base64
from io import BytesIO

class YandexGPT:

    def __init__(self, token, catalog):
        self.token = token
        self.catalog = catalog
        self.url = "https://llm.api.cloud.yandex.net/foundationModels/v1/imageGenerationAsync"

    def generate_image(self, message):
        prompt = {
            "modelUri": f"art://{self.catalog}/yandex-art/latest",
            "generationOptions": {
                "seed": random.randint(1, 1000),
                "ratio": "4:4",
                "aspectRatio": {
                    "widthRatio": "100",
                    "heightRatio": "100"
                }
            },
            "messages": [
                {
                    "weight": 0.4,
                    "text": f"{message}"
                },
                {
                    "weight": 0.6,
                    "text": f"изображение кота"
                }
            ],


        }
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Api-Key {self.token}"
        }

        response = requests.post(self.url, headers=headers, json=prompt)

        print(response.json())

        operation_id = response.json()['id']

        res = None

        for i in range(5):
            res = requests.get(
                f"https://operation.api.cloud.yandex.net/operations/{operation_id}", headers=headers).json()

            if res['done']:
                image_data = base64.b64decode(res['response']['image'])
                image_stream = BytesIO(image_data)
                return image_stream
            time.sleep(3)
        return None


yandex = YandexGPT(settings.YANDEX_API_TOKEN, settings.YANDEX_API_CATALOG)
